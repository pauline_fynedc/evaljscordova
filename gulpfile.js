var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var postcss      = require('gulp-postcss');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');

//browser sync
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});


//convert scss files into css file
gulp.task('sass', function () {
    return gulp.src('www/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('www/css'));
});

//watch file .scss
gulp.task('watch', function () {
    gulp.watch('www/scss/**/*.scss', ['sass']);
});


//auto-prefix
gulp.task('autoprefixer', function () {

    return gulp.src('www/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'));
});

// Default task
gulp.task('default', ['sass', 'watch', 'browser-sync', 'autoprefixer']);

