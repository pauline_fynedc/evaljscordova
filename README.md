### Sujet ###

EVALUTION JAVASCRIPT GOBELIN DDI

### Développeur(s) ###
* Pauline Schnee


### INSTALL / INIT ###
Télecharger et dezipper le dossier cordova envoyé en we-transfert ou telecherger ou coner ce repo

Effectuer les commandes suivantes à la racine du projet :


* $ npm install
* $ cordova platform add android
* $ cordova build android


Pour lancer sur son smartphone :

* $ cordova run android -device --force

(application uniquement testée sous android)


### Bug ?  ###

+ si page blanche et erreur de chargement de ressources externes dans la console, utiliser les sources de ce repo qui contient jquery et tweenMax en local. Cependant, si la connection avec google échoue, il est possible que l'application ne puisse pas actualiser les résultats.


### Concept  ###

* Le concept de l'application est d'encourager l'utilisateur à ne pas utiliser sa voiture sur les trajets du quotidiens en calculant les économies réalisées par l'utilisateur s'il marche / court / fait du vélo par rapport à s'il prennait sa voiture (calcule prix du même trajet en voiture grace aux prix de l'essence et à la consommation moyenne de la voiture).
L'application montre alors à l'utilisateur tout ce qu'il peux acheter comme produits du quotidiens avec les économies qu'il à réalisé.

* Elle montre de la même façon les calories que l'utilisateur à brulés et l'équivalent en therme de réel sport. Pour cela, elle se base sur le poid de l'utilisateur, sa vitesse de déplacement et son activitée courrante (marche, vélo...).

* Le but est d'incrémenter ces résultats à chaques fois que l'utilisateur utilise l'application, afin de créer une sort de score, c'est pourquoi ils sont enregistrés en localStorage.

* possibilité de voir l'évolution par des statistiques



### Technos / Libs ###

* GSAP
* JQUERY -> principalement pour le plugin jQuery touchSwipe
* CANVASJS -> pour les graphiques
* GOOGLE MAP API

* BGLocation Plugin for cordova by pmwisdom (https://github.com/pmwisdom/cordova-background-geolocation-services)



### Utilisation ###

* mode TRACKING : utilise le plugin de géolocalisation pour tracker la position de l'utilisateur. Necessite d'être sur un réel smartphone, d'autoriser la géolocalisation et d'être en déplacement pour voir le résultat

* mode FAKE Tracking : j'ai donc créé un mode générant de fausses données de géolocalisation pour tester l'application, même sans se déplacer. Il génère donc à intervales aléatoires, des données permettant d'update la position de l'utilisateur pour l'application. Le trajet généré est un aller-retour infini entre la plage des Marquisats d'ANNECY et la plage d'Albigny.
ATTENTION : Le mode de déplacement est lui aussi généré aléatoirement. Il se peut alors que le mode détécté soit "IN_VEHICLE". Dans ce cas, le trackage stop. Il faut donc parfois lancer plusieurs fois le mode "fake" pour ne pas être en mode "voiture"



### DEMO ET VISU ###
![16244098_10210113489668007_546128945_n.png](https://bitbucket.org/repo/5Kk8q6/images/1276640016-16244098_10210113489668007_546128945_n.png)
![16216379_10210113489548004_1526796866_n.png](https://bitbucket.org/repo/5Kk8q6/images/4242151349-16216379_10210113489548004_1526796866_n.png)
![16244151_10210113490668032_186863949_n.png](https://bitbucket.org/repo/5Kk8q6/images/3846843041-16244151_10210113490668032_186863949_n.png)