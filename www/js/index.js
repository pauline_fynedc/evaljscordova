
var ViewsManager = function () {
    this.wrapper = document.getElementById("viewsWrapper");
};
ViewsManager.prototype.init = function () {
    this.nav = 'nav';
    this.intro = 'intro';
    this.form = 'form';
    this.app = 'app';

    var view;
    if(app.isUserRegistered){
        view = this.nav;
    }else{
        view = this.intro;
    }

    this.getView(view);

};
ViewsManager.prototype.getView = function (view) {

    var viewPath = 'views/'+view+".html";
    var that = this;

    $.ajax({
        url: viewPath,
        cache:false,
        success: function (data) {
            that.displayView(data);
            that.initView();
        }
    });

};
ViewsManager.prototype.displayView = function (viewHtml) {
    this.wrapper.innerHTML = viewHtml;
};
ViewsManager.prototype.initView = function () {
    delete this.activeView;
    this.activeView = new View(document.querySelector('.view'));
    this.activeView.init();


    //init intro
    if(this.activeView.name == this.intro+"_view"){
        this.activeView.initIntroView();
    }

    //init form
    if(this.activeView.name == this.form+"_view"){
        this.activeView.initFormView();
        app.form = new Form();
        app.form.init(document.querySelector('form'));

    }else{ delete app.form; }


    $('.changeView').on('click', this.bindChangingViewBtn.bind(this));


    //init nav
    if(this.activeView.name == this.nav+"_view"){
        this.activeView.initNavView();
        this.navigation();
    }


    //all except app
    if(this.activeView.name != this.app+"_view" && app.tracker != undefined){
        app.tracker.stopTracking();
        delete app.tracker;
    }


};
ViewsManager.prototype.navigation = function () {
    document.getElementById('startTrack').addEventListener('click', function () {
        this.getView('app');
        setTimeout(function () {
            app.initTracker(true, true);
        },100);

    }.bind(this));

    document.getElementById('useFake').addEventListener('click', function () {
        this.getView('app');
        setTimeout(function () {
            app.initTracker(false, true);
        },100);

    }.bind(this));

    document.getElementById('seeResults').addEventListener('click', function () {
        this.getView('app');
        setTimeout(function () {
            app.initTracker(false, false);
        },100);

    }.bind(this));
};
ViewsManager.prototype.bindChangingViewBtn = function (e) {
    e.preventDefault();
    var targetedView = e.currentTarget.getAttribute('data-view');
    this.getView(targetedView);
};





/*______________________________________________________________________
 * *********************************************************************
 * View
 * @constructor
 */
var View = function (el) {
    this.el = el;
    this.name = this.el.id;
};
View.prototype.init = function () {

    //initializer
    this.initPanels();
    this.setStyles();

    //events
    this.bindEvents();

};
View.prototype.setStyles = function () {

    //animation
    var tl = new TimelineMax();
    tl.from(this.el, 0.3, {'opacity':0, y : 100});


    var width = this.panels.length*100;

    this.panelsSlider = this.el.querySelector('.view-panels');

    this.panelsSlider.style.transition = "";
    this.panelsSlider.style.width = width+'%';

    var translation =  - this.activePanel.getLeftPos();
    this.panelsSlider.style.transform = "translateX("+translation+'px)';
    setTimeout(function () {
        this.panelsSlider.style.transition = "transform 0.5s";
    }.bind(this), 0.1);

};
View.prototype.bindEvents = function () {

    window.addEventListener('resize', this.setStyles.bind(this));

    console.log($(this.panelsSlider).find('.panel'));

    $(this.panelsSlider).find('.panel').swipe({
        swipeLeft:function(event, direction, distance, duration, fingerCount) {
            this.swipePanel("right");
        }.bind(this),

        swipeRight:function(event, direction, distance, duration, fingerCount) {
            this.swipePanel("left");
        }.bind(this)
    });
};
//panel Manager
View.prototype.initPanels = function () {
    this.panels = [];
    var panels = this.el.querySelectorAll('.panel');

    if(this.el.querySelector('.panelNavigation') != undefined){
        this.navigation = true;
        this.navList = this.el.querySelector('.panelNavigation');
    }

    var i=0;
    for(i; i<panels.length; i++){
        panels[i].setAttribute('data-order', i);
        var panel = new Panel(panels[i]);
        panel.init();
        this.panels.push(panel);

        //navigation
        if(this.navigation){
            var navItem = document.createElement('li');
            navItem.className = "panelNavigation-item ";
            navItem.setAttribute('data-panelTarget', i);
            this.navList.appendChild(navItem);
        }

        //set active panel
        if(panel.isActive){
            this.setActivePanel(panel);
            if(this.navigation){
                $(this.navList).find('[data-panelTarget = '+i+']').addClass('active');
            }

        }
    }

    this.panelNavigation();
};
View.prototype.swipePanel = function (direction) {
    var rightPanel;
    var leftPanel;
    var i=0;
    for(i; i<this.panels.length; i++){
        if(this.panels[i].isActive){
            rightPanel = this.panels[i+1];
            leftPanel = this.panels[i-1];
        }
    }


    if(direction == "right" && rightPanel != undefined){
        this.goToPanel(rightPanel);
    }
    if(direction == "left" && leftPanel != undefined){
        this.goToPanel(leftPanel);
    }
};
View.prototype.goToPanel = function (panel) {
    if (panel.parent == this.panelsSlider) {
        this.setActivePanel(panel);
        var x = - panel.getLeftPos();
        this.panelsSlider.style.transform = "translateX("+ x +'px)';

        if(this.navigation){
            $(this.navList).find('.panelNavigation-item').removeClass('active');
            var panelId = panel.el.getAttribute('data-order');
            $(this.navList).find('[data-panelTarget = '+panelId+']').addClass('active')

        }
    }
};
View.prototype.setActivePanel = function (panel) {
    if(this.activePanel != null){
        this.activePanel.unactivate();
    }
    this.activePanel = panel;
    this.activePanel.activate();

    if(this.navigation){

    }
};
View.prototype.panelNavigation = function () {
    var links =  this.el.querySelectorAll('[data-panelTarget]');

    if(links != undefined){
        var i=0;

        var that = this;
        for(i; i<links.length; i++){
            var link = links[i];

            link.addEventListener('click', function () {
                //all links
                var target =  this.getAttribute('data-panelTarget');
                var panelTargeted = that.panels[target];
                that.goToPanel(panelTargeted);

            })
        }

    }

};

//specific
View.prototype.initNavView = function () {

        //elements
        var nav = document.getElementById('mainNavigation');
        var btn = document.querySelector('#startTrack');
        var list = this.el.querySelector('.navList');
        var items = list.querySelectorAll('.navList-item');

        //animations
        var tl = new TimelineLite({delay: 0.2});
        tl.to(btn,1, {'opacity' : 1})
            .from(btn, 1, {scale : 0}, "-=1")
            .from(btn.querySelector('span'), 0.3, {'opacity' : 0}, "-=0.5")
            .from(nav.querySelector('p'), 0.2, {'opacity' : 0})
            .staggerFrom(items, 0.3, {'opacity' : 0, y : 30}, 0.5);

};
View.prototype.initIntroView = function () {

    //elements
    var intro = this.el.querySelector('#intro');
    var titles = intro.querySelectorAll('.title');

    //animations
    var tl = new TimelineLite({delay: 0.5});
     tl.to(intro, 0, {'opacity' : 1})
        .staggerFrom(titles, 0.3, {'opacity' : 0, y : 30}, 0.5)
};
View.prototype.initFormView = function () {
    var backbtn = document.querySelector(".backBtn");
    if(app.isUserRegistered){
        backbtn.setAttribute("data-view", "nav")
    }else{
        backbtn.setAttribute("data-view", "intro")
    }
};





/*______________________________________________________________________
 * *********************************************************************
 * Panel
 * @constructor
 */
var Panel = function (el) {
    this.el = el;
};
Panel.prototype.init = function () {


    this.parent = this.el.parentNode;

    // test active
    if($(this.el).hasClass('active')){
        this.isActive=true;
    }else{
        this.isActive=false;
    }

    //init split section
    if($(this.el).hasClass('split')){
        this.initSplit();
    }

};
Panel.prototype.activate = function () {
    if(!this.isActive){
        $(this.el).addClass('active');
        this.isActive = true;
    }

};
Panel.prototype.unactivate = function () {
    if(this.isActive){
        $(this.el).removeClass('active');
        this.isActive = false;
    }

};
Panel.prototype.getLeftPos = function () {
    return $(this.el).position().left
};
// split panel
Panel.prototype.initSplit = function () {

    //elements
    this.topSection = this.el.querySelector('.splitSection.top');
    this.bottomSection = this.el.querySelector('.splitSection.bottom');
    this.openTopBtn = this.topSection.querySelector('.splitSection-btn');
    this.details = this.topSection.querySelector('.splitSection-details');
    this.main = this.topSection.querySelector('.splitSection-main');
    //variables
    this.middleBreakPoint = 40 +'vh';
    this.bottomBreakPoint = 80 +'vh';


    //anim
    this.tl = new TimelineLite({paused : true});
    this.tl.to(this.topSection, 1, {
        ease: Back.easeOut.config(1.7),
        'height' : this.bottomBreakPoint,
        'clip-path': 'ellipse(120% 100% at 50% 00%)',
        '-webkit-clip-path' :'ellipse(120% 100% at 50% 00%)'
    });
    this.tl.to(this.details, 0.3, {'opacity' : 1}, "-=0.8");
    this.tl.to(this.bottomSection, 1, { ease: Back.easeOut.config(1.7),'transform' : "translate3d(0, "+ this.bottomBreakPoint+", 0)"}, "-=1.1");
    this.tl.to( this.el, 0, {'overflow-y' : 'hidden'});


    //events
    this.flag = false;
    this.openTopBtn.addEventListener('click', this.togggleBtn.bind(this));
    $(this.topSection).swipe({
        swipeDown:function(event, direction, distance, duration, fingerCount) {
            this.openTopSection();
        }.bind(this),

        swipeUp:function(event, direction, distance, duration, fingerCount) {
            this.closeTopSection();
        }.bind(this)
    });
    //this.el.addEventListener('scroll', this.squeezeTopSection.bind(this));

};
Panel.prototype.togggleBtn = function (e) {
    e.preventDefault();
    if(!this.flag) {
        this.openTopSection()
    }else{
        this.closeTopSection()
    }

};
Panel.prototype.openTopSection = function () {

    if(!this.flag) {

        this.tl.play();

        //fadeOut btn
        this.openTopBtn.querySelector('p').style.opacity = 0;
        $(this.openTopBtn.querySelector('.chevron')).addClass('close');


        this.flag = true;
    }
};
Panel.prototype.closeTopSection = function () {

    if(this.flag) {

        this.tl.reverse();

        //fadeIn btn
        this.openTopBtn.querySelector('p').style.opacity = 1;
        $(this.openTopBtn.querySelector('.chevron')).removeClass('close');


        this.flag = false;
    }
};
Panel.prototype.squeezeTopSection = function (e) {

    var top = this.bottomSection.getBoundingClientRect().top - 50 /* header height */;

    if(top > 100){
        //section squeeze
        this.topSection.style.transition = "none";
        this.topSection.style.height = top+"px";
        this.topSection.style.clipPath = "";
        this.topSection.querySelector('.mainData').style.transform ="";

    }else{
        this.topSection.style.transition = "height 0.3s, clip-path 0.8s";
        this.topSection.style.clipPath = "ellipse(1000% 100% at 50% 00%)";

        this.topSection.querySelector('.mainData').style.transform = "scale(0.5) translateY(-44%)";
    }

};

