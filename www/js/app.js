

var app = {


    datas : {
        car : {
            fuel : "",
            price :null,
            conso : null
        },
        user : {
            sexe : "",
            age : null,
            weight : null
        },
        distance : 0,
        saving : 0,
        calories : 0,
        historic:[]
    },


    // Application Constructor
    initialize: function() {
        this.bindEvents();

        this.initLocalStorage();
        this.initViews();
    },

    // Bind Event Listeners
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    onDeviceReady: function() {
        console.log('device ready');

        /*this.initLocalStorage();
        this.initViews();*/
    },

    // LocalStorage initializer
    initLocalStorage : function () {
        this.localStorage = new LocalStorage();
        this.isUserRegistered = this.localStorage.check();

        if(this.isUserRegistered){
            this.datas = this.localStorage.getAll();
        }
    },

    // ViewsManager initializer
    initViews : function () {
        this.viewsManager = new ViewsManager();
        this.viewsManager.init();
    },

    //Tracker initializer
    initTracker:function (forReal, allowTracking) {
        console.log('initTracker');
        this.tracker = new Tracker();
        this.tracker.init(forReal, allowTracking);
    }

};


/*_______________________________________________________________________________
 ********************************************************************************
 * Tracker
 * Manage app trackings datas : locations, distances, savings, burned Calories
 * @constructor
 */
var Tracker = function () {
    this.locations = [];
    this.origin = this.locations[0];
};
//initialize
Tracker.prototype.init = function (forReal, allowTracking) {

    this.forReal = forReal;
    this.allowTracking = allowTracking;


    //check currentPosition on start
    navigator.geolocation.getCurrentPosition(function(location) {
        this.addLocationPoint(location.timestamp, location.coords.latitude, location.coords.longitude);
    }.bind(this), function(error) {
        console.error(error);
        alert("Impossible de vous géolocaliser");
    });


    //elements initialize
    this.initData();
    this.initGraphs();
    this.initResults();

    //display
    this.displayTotalDistance();
    this.displayTotalSaving();
    this.displayTotalBurnedCalories();

    //if mode track activited (not just display datas)
    if(this.allowTracking){
        this.initTrack()
    }


};
Tracker.prototype.initData = function () {

    /*get datas from local storage*/

    //get score
    this.totalDistance = app.datas.distance;
    this.totalSaving = app.datas.saving;
    this.totalBurnedCalorie = parseFloat(app.datas.calories);
    this.historic = app.datas.historic;

    //get user's data
    this.userWeight = app.datas.user.weight;

    // get car's data & init car
    var fuel = app.datas.car.fuel;
    var conso = app.datas.car.conso;
    var price = app.datas.car.price;
    this.car = new Car();
    this.car.init(fuel, conso, price);

};
Tracker.prototype.initGraphs = function () {
    this.savingGraph = new Graph('saving', 'économies réalisée');
    this.calGraph = new Graph('calorie', 'calories brûlées');
};
Tracker.prototype.initTrack = function () {

    if(this.forReal){
        /* real tracking mode */
        this.track();
    }else{
        /* fake tracking mode */
        this.generateFakeLocations();
        this.generateFakeActivities();
    }
};
Tracker.prototype.initResults = function () {

    this.productManager = new ProductManager();
    this.productManager.init(this.totalSaving);

    this.activityManager = new ActivityManager();
    this.activityManager.init(this.totalBurnedCalorie);
};
//track
Tracker.prototype.track = function () {

    /*REAL TRACKER MODE*/


    //Get and configure bgLocationService plugin
    this.bgLocationServices =  window.plugins.backgroundLocationServices;
    this.bgLocationServices.configure({
        //Both
        desiredAccuracy: 1, // Desired Accuracy of the location updates (lower means more accurate but more battery consumption)
        distanceFilter: 10, // (Meters) How far you must move from the last point to trigger a location update
        debug: true, // <-- Enable to show visual indications when you receive a background location update
        interval: 9000, // (Milliseconds) Requested Interval in between location updates.
        useActivityDetection: true, // Uses Activitiy detection to shut off gps when you are still (Greatly enhances Battery Life)

        //Android Only
        notificationTitle: 'BG Plugin', // customize the title of the notification
        notificationText: 'Tracking', //customize the text of the notification
        fastestInterval: 5000 // <-- (Milliseconds) Fastest interval your app / server can handle updates

    });

    //on location updates
    this.bgLocationServices.registerForLocationUpdates(function (location) {
        this.onLocationUpdate(location);
    }.bind(this), function(err) {
        console.log("Error: Didnt get an update", err);
    });

    //on Activity Updates
    this.bgLocationServices.registerForActivityUpdates(function(activities) {
        this.getActivity(activities);
    }.bind(this), function(err) {
        console.log("Error: Something went wrong", err);
        alert('impossible de connaître votre activitée')
    });

    //Start the Background Tracker
    this.bgLocationServices.start();
};
Tracker.prototype.onLocationUpdate = function (location) {
    /*when receive a new location*/

    var lastLocation = this.currentLocation;
    this.addLocationPoint(location.timestamp, location.latitude, location.longitude);

    var currentLocation = this.currentLocation;
    this.getDistanceBetweenTwoPoint(lastLocation, currentLocation);
};
Tracker.prototype.addLocationPoint = function (timestamp, lat, lng) {
    this.currentLocation = new LocationPoint(timestamp, lat, lng);
    this.locations.push(this.currentLocation);
};
//get
Tracker.prototype.getDistanceBetweenTwoPoint = function (point1, point2) {

    var that = this;
    var orig = {lat: point1.lat, lng: point1.lng};
    var dest = {lat: point2.lat, lng: point2.lng};

    //get duree between the two point
    var duree = point2.time - point1.time;
    duree = (duree/1000)/3600;    // Nombre de heures entre les 2 dates
    duree = duree % 24;

    /*USE GOOGLE MAP DISTANCE MATRIX SERVICES
    to get the real distance between two point */
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: [orig],
            destinations: [dest],
            travelMode: google.maps.TravelMode.WALKING,
            unitSystem: google.maps.UnitSystem.METRIC
        }, callback);

    function callback(response, status){
        var distance = response.rows[0].elements[0].distance.value;

        //get burned calories
        that.getBurnedCalorieBetweenTwoPoint(distance, duree, that.currentActivity);

        //update total distance
        that.updateTotalDistance(distance);
        return distance;
    }
};
Tracker.prototype.getBurnedCalorieBetweenTwoPoint = function (distance, time, activity) {

    //get deplacement speed
    var speed = (distance / 1000 ) / time;
    console.log(time + "h");
    console.log(distance/1000 + "km");
    console.log(speed + " km/h");

    //Get calorie burned coefficient : depend on current activity and speed (coeff = MET constant)
    var coeff = 0;
    if(activity == "ON_FOOT" || activity == "WALKING" || activity == "RUNNING" || activity == "UNKNOWN"){
       if(speed < 3*1.6 && speed>=0){
           coeff = 3;
       }else if(speed<4*1.6){
           coeff = 4;
       } else if(speed<5*1.6){
           coeff = 4.5;
       }if(speed < 5.5*1.6){
            coeff = 8;
        }else if(speed<6*1.6){
            coeff = 10;
        } else if(speed<7*1.6){
            coeff = 11;
        } else if(speed<8*1.6){
            coeff = 12.5;
        } else if(speed<9*1.6){
            coeff = 15;
        } else if(speed<10*1.6){
            coeff = 16;
        }else{
            coeff = 18;
        }
    }
    if(activity == "ON_BICYCLE"){
        if(speed < 10 && speed>=0){
            coeff = 4;
        }else if(speed<12*1.6){
            coeff = 6;
        } else if(speed<14*1.6){
            coeff = 8;
        } else if(speed<16*1.6){
            coeff = 10;
        } else if(speed<20*1.6){
            coeff = 12;
        }else{
            coeff = 16;
        }
    }
    if(activity == "STILL"){
        coeff = 0;
    }
    if(activity == "IN_VEHICLE"){
        coeff = 0;
    }

    //burned calorie formula
    var burnedCalories  = coeff * this.userWeight * time;  //calories/min
    this.updateTotalBurnedCalories(burnedCalories);

};
Tracker.prototype.getActivity = function (activities) {
    var lastConfidence = 0;
    var confidence;
    var activity;

    //get current activity from activity precision tab returned by android DetectedActivityService
    for (var i in activities){
        if( activities[i] > lastConfidence){
            activity = i;
            confidence = activities[i];
        }
        lastConfidence = activities[i];
    }

    this.currentActivity = activity;
    this.displayCurrentActivity();



    //STOP TRACKING IF detected activity is "IN VEHICULE"
    if(this.currentActivity == 'IN_VEHICLE'){
        setTimeout(function () {
            alert("Tricheur ! Vous ne ferez pas d'économies en polluant ! ;) ");
            app.changeView(document.getElementById('nav_view'));
        }, 1000);

    }


};
//display
Tracker.prototype.displayTotalDistance = function () {
    $('.distance').html(this.totalDistance/1000);
};
Tracker.prototype.displayTotalSaving =function () {
    document.getElementById('economies').innerHTML = Math.round(this.totalSaving*100)/100
};
Tracker.prototype.displayTotalBurnedCalories = function () {
    document.getElementById('calories').innerHTML = Math.round(this.totalBurnedCalorie)
};
Tracker.prototype.displayCurrentActivity = function () {
    document.getElementById('current-activity').style.backgroundImage = 'url(images/'+this.currentActivity+'.svg)';
};
//update
Tracker.prototype.updateTotalDistance = function (newDistance) {

    /*updat only if user is not driving*/
    if(this.currentActivity != 'IN_VEHICLE'){

        this.totalDistance+= newDistance;
        this.displayTotalDistance();

        //store new data
        app.datas.distance = this.totalDistance;
        app.localStorage.setAll(app.datas);

        this.updateTotalSaving(this.totalDistance);
    }

};
Tracker.prototype.updateTotalSaving = function (distance) {
    var conso = this.car.getConso(distance);

    this.totalSaving = this.car.getPrice(conso);
    this.displayTotalSaving();

    //store it
    app.datas.saving = this.totalSaving;
    app.localStorage.setAll(app.datas);

    //get new available product list
    this.productManager.getAvailableProducts(this.totalSaving);

    //add this to history
    this.addToHistoric();
};
Tracker.prototype.updateTotalBurnedCalories = function (newBurnedCalories) {
    this.totalBurnedCalorie += newBurnedCalories;
    this.displayTotalBurnedCalories();

    //store it
    app.datas.calories = this.totalBurnedCalorie;
    app.localStorage.setAll(app.datas);

    //get sports equivalent list
    this.activityManager.getAvailableActivities(this.totalBurnedCalorie);

    //add this to historic
    this.addToHistoric();
};
Tracker.prototype.addToHistoric = function () {
    var data = {
        date : this.currentLocation.time,
        distance : this.totalDistance,
        saving : this.totalSaving,
        calorie : this.totalBurnedCalorie
    };

    app.datas.historic.push(data);
    app.localStorage.setAll(app.datas);


    this.savingGraph.update(data);
    this.calGraph.update(data);

};
//debug
Tracker.prototype.generateFakeLocations = function () {
    var itineraire = [
        {timestamp: Date.now(), latitude: 45.89445, longitude: 6.134534},
        {timestamp: Date.now(), latitude: 45.89545, longitude: 6.133332},
        {timestamp: Date.now(), latitude: 45.896914, longitude: 6.131766},
        {timestamp: Date.now(), latitude: 45.898318, longitude: 6.128976},
        {timestamp: Date.now(), latitude: 45.900976, longitude: 6.127539},
        {timestamp: Date.now(), latitude: 45.902245, longitude: 6.131465},
        {timestamp: Date.now(), latitude: 45.902717, longitude: 6.143353}
    ];

    var i=0;
    var flag = 0;
    var random=(Math.random() * 10000) + 3000;
    this.fakeTracker = setInterval(function() {

        itineraire[i].timestamp = Date.now();

        var point = itineraire[i];
        random = (Math.random() * 10000) + 3000;
        this.onLocationUpdate(point);

        if(flag%2 == 0 && i<itineraire.length){
            i++;
        }else if(flag%2 != 0 && i<itineraire.length){
            i--;
        }
        if(i==itineraire.length-1){
            flag +=1;
        }
        if(i==0){
            flag +=1;
        }

    }.bind(this),random);
};
Tracker.prototype.generateFakeActivities = function () {
    var activities = {
        'STILL' : Math.floor(Math.random()*100),
        'ON_FOOT' : Math.floor(Math.random()*100),
        'ON_BICYCLE' : Math.floor(Math.random()*100),
        'IN_VEHICLE' : Math.floor(Math.random()*10)
    };

    this.getActivity(activities);
};
//stop
Tracker.prototype.stopTracking = function () {
    if(this.forReal){
        this.bgLocationServices.stop();
    }else{
        clearInterval(this.fakeTracker);
    }
};




/*_______________________________________________________________________________
 ********************************************************************************
 * gpsPoint
 * @param time
 * @param lat
 * @param lng
 */
var LocationPoint = function (time, lat, lng) {
    this.time=time;
    this.lat=lat;
    this.lng=lng;
};
LocationPoint.prototype.display=function () {


    /*Display a list of GPS POints detected for debugage*/

    var point = document.createElement('ul');

    //timestamp
    var time = document.createElement('li');
    time.innerHTML="<span class='label'>timestamp : </span>"+"<span class='label'>"+this.time+"</span>";
    point.appendChild(time);

    //latitude
    var lat = document.createElement('li');
    lat.innerHTML="<span class='label'>latitude : </span>"+"<span class='label'>"+this.lat+"</span>";
    point.appendChild(lat);

    //longitude
    var lng = document.createElement('li');
    lng.innerHTML="<span class='label'>longitude : </span>"+"<span class='label'>"+this.lng+"</span>";
    point.appendChild(lng);


    document.getElementById('locations-list').appendChild(point);
};






/*_______________________________________________________________________________
 ********************************************************************************
 * Graph
 * @param dataType : saving or activity
 * @param title : title of y axis
 * @constructor
 * use CANVASJS
 */
var Graph = function (dataType, title) {
    this.dataType = dataType;
    this.title = title;
    this.initData();
    window.onload = this.init();
};
Graph.prototype.initData = function () {
    var historic = app.localStorage.getAll().historic;

    this.datas = [];

    var i = 0;
    for(i; i< historic.length; i++){
        var point = {
            x : new Date(historic[i].date),
            y : historic[i][this.dataType]
        };
        this.datas.push(point);
    }

};
Graph.prototype.init = function () {


    this.chart = new CanvasJS.Chart("graph-"+this.dataType,
        {

            backgroundColor: "",
            culture: "fr",
            toolTip:{
                enabled: false   //enable here
            },
            axisX:{
                title: "temps",
                gridThickness: 1,
                gridColor: "rgba(255,255,255,0.15",
                titleFontFamily: "Lato",
                titleFontWeight: "300",
                titleFontSize: 15,
                titleFontColor: "white",
                lineColor: "white",
                tickLength: 0,
                labelFontColor: "white",
                labelFontSize: 12,
                margin: 30 ,
                labelAngle: -22
            },
            axisY: {
                title: this.title,
                gridThickness: 1,
                gridColor: "rgba(255,255,255,0.15",
                titleFontFamily: "Lato",
                titleFontWeight: "300",
                titleFontSize: 15,
                titleFontColor: "white",
                lineColor: "white",
                tickLength: 0,
                labelFontColor: "white"
            },
            data: [
                {
                    type: "line",
                    lineColor:"white",
                    markerType: "none",
                    dataPoints: this.datas
                }
            ]
        });

    this.chart.render();

};
Graph.prototype.update = function (point) {

    var length = this.chart.options.data[0].dataPoints.length;
     this.chart.options.data[0].dataPoints.push(
         {
            x : new Date(point.date),
            y : point[this.dataType]
         }
     );
     this.chart.render();
};





/*_______________________________________________________________________________
 ********************************************************************************
 * Form
 * @constructor
 */
var Form = function () {};
Form.prototype.init = function (el) {
    this.el = el;
    this.submitBtn = document.getElementById('submitForm');

    //if user already exist, edit mode
    if(app.isUserRegistered){
        this.fill();
        this.submitBtn.innerHTML = "Modifier";
    }

    //get form items values
    this.getResponses();

    //event associated to form
    this.bindEvents();

};
Form.prototype.bindEvents = function () {
    //submit
    this.submitBtn.addEventListener('click', this.submit.bind(this));

    //on checkbox click
    $(this.el).find('.checkbox').click(function (e) {
        this.checkboxManager(e.currentTarget)
    }.bind(this));

    //on select change
    $(this.el).find('#gaz').on( "change", function () {
        this.checkboxManager(document.querySelector('[data-reference=gazPrice]'));
        this.checkboxManager(document.querySelector('[data-reference=gazConso]'));
    }.bind(this));
};
Form.prototype.submit = function (e) {
    e.preventDefault();

    this.getResponses();

    //check if all recommanded items are valid
    if(this.isValide() != false){
        this.setUserData();
        app.isUserRegistered = app.localStorage.check();
        app.viewsManager.getView('nav');
    }
    else{
        alert("vous devez remplir tous les champ ou mettre des valeurs valides")
    }

};
Form.prototype.setUserData = function () {

    app.datas.car = {
        fuel : this.gaz,
        price : this.gazPrice,
        conso : this.gazConso
    };

    app.datas.user = {
        sexe : this.sexe,
        age : this.age,
        weight : this.weight
    };

    app.localStorage.setAll(app.datas);

};
Form.prototype.getResponses = function () {
    var gaz =  document.getElementById("gaz");
    this.gaz = gaz.options[gaz.selectedIndex].value;
    this.gazPrice = document.getElementById("gazPrice").value;
    this.gazConso = document.getElementById("gazConso").value;

    var sexe =  document.getElementById("sexe");
    this.sexe = sexe.options[sexe.selectedIndex].value;
    this.age = document.getElementById("age").value;
    this.weight = document.getElementById("weight").value;
};
Form.prototype.checkboxManager = function (checkbox) {
    /*associate a checkbox with his associated input*/

    var el = checkbox;
    var target = el.getAttribute('data-reference');
    var gaz =  document.getElementById("gaz");
    gaz = gaz.options[gaz.selectedIndex].value;

    input = document.getElementById(target);

    if($(el).is(':checked')){
        if(target == "gazPrice"){
           this.setInput(input, getfuelPrice(gaz))
        }
        if(target == "gazConso"){
            this.setInput(input, getfuelConso(gaz))
        }
        input.setAttribute('disabled', true);
    }else{
        this.clearInput(input);
        input.removeAttribute('disabled');
    }
};
Form.prototype.fill = function () {

    var gazOptions =  document.getElementById("gaz").querySelectorAll('option');
    var i =0;
    for(i; i<gazOptions.length; i++){
        gazOptions[i].removeAttribute('selected');
        if(gazOptions[i].value == app.datas.car.fuel){
            gazOptions[i].setAttribute('selected', '');
        }
    }

    this.setInput(document.getElementById("gazPrice"), app.datas.car.price);
    this.setInput(document.getElementById("gazConso"), app.datas.car.conso);

    var sexeOptions =  document.getElementById("sexe").querySelectorAll('option');
    var j =0;
    for(j; j<sexeOptions.length; j++){
        sexeOptions[j].removeAttribute('selected');
        if(sexeOptions[j].value == app.datas.user.sexe){
            sexeOptions[j].setAttribute('selected', '');
        }
    }

    this.setInput(document.getElementById("age"), app.datas.user.age);
    this.setInput(document.getElementById("weight"), app.datas.user.weight)

};
Form.prototype.isValide = function () {
    if(this.gaz == "" || this.sexe == "" || this.gazPrice == "" || this.gazPrice == 0 || this.gazConso == "" || this.gazConso == 0  || this.weight == "" || this.weight == 0){
        return false
    }
   else{
       return true;
    }
};
//inputs
Form.prototype.setInput = function(input, value){
    input.value = value;
    if(value != "" && value != undefined && value != null){
        $(input).addClass('valid');
    }
};
Form.prototype.clearInput = function (input) {
    input.value = "";
    $(input).removeClass('valid');
};





/*_______________________________________________________________________________
 ********************************************************************************
 * Local Storage
 * @constructor
 */
var LocalStorage = function () {};
LocalStorage.prototype.check = function () {
    if(typeof window.localStorage.getItem("appData") == undefined || window.localStorage.getItem("appData") == null ){
        console.log('pas encore user');
        return false;
    }else{
        return true
    }
};
LocalStorage.prototype.setAll = function (datas) {
    var string = JSON.stringify(datas);
    window.localStorage.setItem("appData", string);
};
LocalStorage.prototype.getAll = function () {
    var stringify =  window.localStorage.getItem("appData");
    var datas = JSON.parse(stringify);
    return datas
};







/*_______________________________________________________________________________
 ********************************************************************************
 * Product Manager
 * @constructor
 */

var ProductManager = function () {};
ProductManager.prototype.init = function (maxPrice) {
    this.getProductList();
    this.maxPrice = maxPrice;
};
ProductManager.prototype.getProductList = function () {
    var req = new XMLHttpRequest();
    req.open('GET', 'datas/product.json', true);
    req.addEventListener('readystatechange', function(e){
        if (req.readyState == 4){
            if(req.status == 200){
                this.products = JSON.parse(req.responseText);
                this.generateRandoms();
                this.getAvailableProducts(this.maxPrice)
            } else {
                console.log('Erreur');
                console.log(req.status);
            }
        } else {
            switch(req.readyState){
                case 0:
                    console.log('init');
                    break;

                case 1:
                    console.log('open');
                    break;

                case 2:
                    console.log('sended');
                    break;

                case 3:
                    console.log('got');
                    break;
            }
        }
    }.bind(this));
    req.send();
};
ProductManager.prototype.getAvailableProducts = function (max) {

    this.availableProducts = [];
    var max = Math.round(max*100)/100;
    var productsList = this.products;

    for(var key in productsList){

        //var product = new Product(key, productsList[key].desc, productsList[key].price);

        var product = {
            id : key,
            desc : productsList[key].desc,
            price : productsList[key].price,
            quantity: null,
            imgPath : key+".svg"
        };

        if(product.price <= max ){

            var nb = Math.floor(max / product.price);
            product.quantity = nb;

            if(nb > 1){
                product.desc = productsList[key].descPluriel;
            }

            this.availableProducts.push(product);

        }
    }

    this.displayProductList();
    return this.availableProducts;

};
ProductManager.prototype.displayProductList = function () {

    var list = document.getElementById('productList');

    list.innerHTML = "";

    if(this.availableProducts.length == 0){
        list.parentNode.querySelector('.indication--empty').style.display = "block";
        list.parentNode.querySelector('.indication--notempty').style.display = "none";
    }else{
        list.parentNode.querySelector('.indication--empty').style.display = "none";
        list.parentNode.querySelector('.indication--notempty').style.display = "block";
    }

    var i=0;
    for(i; i< this.availableProducts.length; i++){
        var item = document.createElement('li');
        item.className = "resultsList-item";

        item.innerHTML = "<p><span class='strong'>"+this.availableProducts[i].quantity+"</span> "+this.availableProducts[i].desc+"</p>";
        item.style.backgroundImage = "url(images/"+this.availableProducts[i].imgPath+")";

        item.style.backgroundPositionX = this.randoms[i] + "%";


        list.appendChild(item);
    }

};
ProductManager.prototype.generateRandoms = function () {
    this.randoms = [];
    var size = 0, key;
    for (key in this.products) {
        var random=(Math.random() * 100);
        this.randoms.push(random);
    }


};





/*_______________________________________________________________________________
 ********************************************************************************
 * Activity Manager
 * @constructor
 */
var ActivityManager = function () {};
ActivityManager.prototype.init = function(burnedCal){
    this.getActivityList();
    this.burnedCal = burnedCal;
};
ActivityManager.prototype.getActivityList = function () {
    var req = new XMLHttpRequest();
    req.open('GET', 'datas/activities.json', true);
    req.addEventListener('readystatechange', function(e){
        if (req.readyState == 4){
            if(req.status == 200){
                this.activities = JSON.parse(req.responseText);
                this.generateRandoms();
                this.getAvailableActivities(this.burnedCal)
            } else {
                console.log('Erreur');
                console.log(req.status);
            }
        } else {
            switch(req.readyState){
                case 0:
                    console.log('init');
                    break;

                case 1:
                    console.log('open');
                    break;

                case 2:
                    console.log('sended');
                    break;

                case 3:
                    console.log('got');
                    break;
            }
        }
    }.bind(this));
    req.send();
};
ActivityManager.prototype.getAvailableActivities = function (burnedCal) {

    this.availableActivities = [];
    var activitiesList = this.activities;

    for(var key in activitiesList){

        var activity = {
            id : key,
            desc : activitiesList[key].desc,
            cal : this.getActivityCal(activitiesList[key]),
            duree: null,
            imgPath : key+".svg"
        };

        var duree = burnedCal / activity.cal;
        activity.duree = Math.floor(duree * 15);


        if(activity.duree >= 5){
            this.availableActivities.push(activity);
        }
    }

    this.displayActivitiesList();
    return this.availableProducts;

};
ActivityManager.prototype.displayActivitiesList = function () {
    var list = document.getElementById('activityList');

    list.innerHTML = "";


    if(this.availableActivities.length == 0){
        list.parentNode.querySelector('.indication--empty').style.display = "block";
        list.parentNode.querySelector('.indication--notempty').style.display = "none";
    }else{
        list.parentNode.querySelector('.indication--empty').style.display = "none";
        list.parentNode.querySelector('.indication--notempty').style.display = "block";
    }

    var i=0;
    for(i; i< this.availableActivities.length; i++){
        var item = document.createElement('li');
        item.className = "resultsList-item";

        item.innerHTML = "<p><span class='strong'>"+this.availableActivities[i].duree+" min</span> "+this.availableActivities[i].desc+"</p>";
        item.style.backgroundImage = "url(images/"+this.availableActivities[i].imgPath+")";

        item.style.backgroundPositionX = this.randoms[i] + "%";


        list.appendChild(item);
    }
};
ActivityManager.prototype.getActivityCal = function (data) {


    var cal;
    var weight = app.datas.user.weight;

    if(weight<=60){
        cal = data.cal60
    }else if(weight>60 && weight < 85){
        cal =  data.cal70
    }else if(weight<=85){
        cal = data.cal85
    }

    return cal;

};
ActivityManager.prototype.generateRandoms = function () {
    this.randoms = [];
    var size = 0, key;
    for (key in this.activities) {
        var random=(Math.random() * 100);
        this.randoms.push(random);
    }


};





/*_______________________________________________________________________________
 ********************************************************************************
 * Car
 * @param fuel
 * @constructor
 */
var Car = function () {

    this.fuel = {
        type : null,
        conso : null,
        price : null
    }
};
Car.prototype.init = function (fuel, conso, price) {
    this.fuel.type = fuel;
    this.fuel.conso = conso;
    this.fuel.price = price;

};
Car.prototype.getConso = function (distance) { //distance en m - Conso en L
    return ((this.fuel.conso/100)*distance)/1000
};
Car.prototype.getPrice = function (conso) { //Conso en L - prix en €
    return (this.fuel.price*conso)
};




/*_______________________________________________________________________________
 *******************************************************************************/

function getfuelConso(fuel) {
    switch(fuel){
        case "SP95" :
            return 7.42;
            break;
        case "SP98" :
            return 7.42;
            break;
        case "gazole" :
            return 6.16;
            break;
        case "autre" :
            return 6.49;
            break;
    }
}
function getfuelPrice(fuel) {
    switch(fuel){
        case "SP95" :
            return 1.413;
            break;
        case "SP98" :
            return 1.479;
            break;
        case "gazole" :
            return 1.275;
            break;
        case "autre" :
            return 1.3;
            break;
    }
}




app.initialize();
